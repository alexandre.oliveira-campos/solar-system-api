using System.Collections.Generic;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public interface IPlanetService
    {
        IList<Planet> GetAll();
        Planet GetSingle(int id);
        Planet AddPlanet(Planet newPlanet);
        Planet Update(int id, Planet newPlanet);
        void Delete(int id);
    }
}
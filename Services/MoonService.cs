using System.Collections.Generic;
using System.Linq;
using SolarSystemApi.Data;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public class MoonService : IMoonService
    {
        private readonly SolarSystemApiDataContext _context;

        public MoonService(SolarSystemApiDataContext context)
        {
            _context = context;
        }

        public IList<Moon> GetAll()
        {
            return _context.Moons.ToList();
        }
        
        public Moon GetSingle(int id)
        {
            return _context.Moons.FirstOrDefault(p => p.Id == id);
        }
        
        public Moon AddMoon(Moon newMoon)
        {
            _context.Moons.Add(newMoon);
            
            var planet = _context.Planets.FirstOrDefault(p => p.Id == newMoon.PlanetId);
            planet.Moons.Add(newMoon);

            _context.Planets.Update(planet);
            
            _context.SaveChanges();
            return newMoon;
        }
        
        public Moon Update(int id, Moon newMoon)
        {
            var moon = _context.Moons.FirstOrDefault(m => m.Id == id);
            
            moon.Name = newMoon.Name;

            _context.SaveChanges();

            return moon;
        }

        public void Delete(int id)
        {
            _context.Moons.Remove(_context.Moons.FirstOrDefault(m => m.Id == id));
            _context.SaveChanges();
        }
    }
}
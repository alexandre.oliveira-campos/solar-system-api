using System.Collections.Generic;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public interface IMoonService
    {
        IList<Moon> GetAll();
        Moon GetSingle(int id);
        Moon AddMoon(Moon newMoon);
        Moon Update(int id, Moon newMoon);
        void Delete(int id);
    }
}
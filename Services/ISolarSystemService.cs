using System.Collections.Generic;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public interface ISolarSystemService
    {
        IList<SolarSystem> GetAll();
        SolarSystem GetSingle(int id);
        SolarSystem AddSolarSystem(SolarSystem newSolarSystem);
        SolarSystem Update(int id, SolarSystem newSolarSystem);
        void Delete(int id);
    }
}
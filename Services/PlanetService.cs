using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SolarSystemApi.Data;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public class PlanetService : IPlanetService
    {
        private readonly SolarSystemApiDataContext _context;

        public PlanetService(SolarSystemApiDataContext context)
        {
            _context = context;
        }

        public IList<Planet> GetAll()
        {
            return _context.Planets.Include(p => p.Moons).ToList();
        }
        
        public Planet GetSingle(int id)
        {
            return _context.Planets.Include(p => p.Moons).FirstOrDefault(p => p.Id == id);
        }
        
        public Planet AddPlanet(Planet newPlanet)
        {
            _context.Planets.Add(newPlanet);
            
            var solarSystem = _context.SolarSystems.FirstOrDefault(s => s.Id == newPlanet.SolarSystemId);
            solarSystem.Planets.Add(newPlanet);

            _context.SolarSystems.Update(solarSystem);
            
            _context.SaveChanges();
            return newPlanet;
        }
        
        public Planet Update(int id, Planet newPlanet)
        {
            var planet = _context.Planets.FirstOrDefault(p => p.Id == id);
            
            planet.Name = newPlanet.Name;
            planet.Size = newPlanet.Size;
            planet.Color = newPlanet.Color;

            _context.SaveChanges();

            return planet;
        }

        public void Delete(int id)
        {
            _context.Planets.Remove(_context.Planets.FirstOrDefault(p => p.Id == id));
            _context.SaveChanges();
        }
    }
}
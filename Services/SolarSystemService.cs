using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SolarSystemApi.Data;
using SolarSystemApi.Models;

namespace SolarSystemApi.Services
{
    public class SolarSystemService : ISolarSystemService
    {
        private readonly SolarSystemApiDataContext _context;

        public SolarSystemService(SolarSystemApiDataContext context)
        {
            _context = context;
        }

        public IList<SolarSystem> GetAll()
        {
            return _context.SolarSystems.Include(s => s.Planets).ToList();
        }
        
        public SolarSystem GetSingle(int id)
        {
            return _context.SolarSystems.Include(s => s.Planets).FirstOrDefault(s => s.Id == id);
        }
        
        public SolarSystem AddSolarSystem(SolarSystem newSolarSystem)
        {
            _context.SolarSystems.Add(newSolarSystem);
            _context.SaveChanges();
            return newSolarSystem;
        }
        
        public SolarSystem Update(int id, SolarSystem newSolarSystem)
        {
            var solarSystem = _context.SolarSystems.FirstOrDefault(s => s.Id == id);
            
            solarSystem.Name = newSolarSystem.Name;

            _context.SaveChanges();

            return solarSystem;
        }
        
        public void Delete(int id)
        {
            _context.SolarSystems.Remove(_context.SolarSystems.FirstOrDefault(s => s.Id == id));
            _context.SaveChanges();
        }
    }
}
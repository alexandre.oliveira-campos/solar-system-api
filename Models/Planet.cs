using System.Collections.Generic;

namespace SolarSystemApi.Models
{
    public class Planet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Color { get; set; }
        public int SolarSystemId { get; set; }
        public List<Moon> Moons { get; set; }
    }
}
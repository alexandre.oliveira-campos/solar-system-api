using System.Collections.Generic;

namespace SolarSystemApi.Models
{
    public class SolarSystem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Planet> Planets { get; set; }
    }
}
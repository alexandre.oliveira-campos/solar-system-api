using Microsoft.AspNetCore.Mvc;
using SolarSystemApi.Models;
using SolarSystemApi.Services;

namespace SolarSystemApi.Controllers
{
    [ApiController]
    public class SolarSystemController : ControllerBase
    {
        private readonly ISolarSystemService _solarSystemService;

        public SolarSystemController(ISolarSystemService solarSystemService)
        {
            _solarSystemService = solarSystemService;
        }
        
        [HttpGet("solar-systems")]
        public IActionResult GetAll()
        {
            return Ok(_solarSystemService.GetAll());
        }
        
        [HttpGet("solar-systems/{id}")]
        public IActionResult GetSingle(int id)
        {
            if (id < 1) return BadRequest();
            var solarSystem = _solarSystemService.GetSingle(id);
            if (solarSystem == null) return NotFound();
            return Ok(solarSystem);
        }
        
        [HttpPost("solar-systems/new")]
        public IActionResult AddSolarSystem(SolarSystem newSolarSystem)
        {
            if (newSolarSystem.Id < 1) return BadRequest();
            //if (_solarSystemService.ExistsByName(newSolarSystem.Name)) return BadRequest();
            //if (_solarSystemService.ExistsById(newSolarSystem.Id)) return BadRequest();

            _solarSystemService.AddSolarSystem(newSolarSystem);

            return Created($"solar-systems/{newSolarSystem.Id}/edit", newSolarSystem);
        }
        
        [HttpPost("solar-systems/{id}/edit")]
        public IActionResult Update([FromRoute] int id, [FromBody] SolarSystem newSolarSystem)
        {
            if (id < 1) return BadRequest();
            //if (_solarSystemService.ExistsByName(newSolarSystem.Name)) return BadRequest();
            //if (!_solarSystemService.ExistsById(id)) return NotFound();

            return Ok(_solarSystemService.Update(id, newSolarSystem));
        }
        
        [HttpDelete("solar-systems/{id}/delete")]
        public IActionResult Delete(int id)
        {
            _solarSystemService.Delete(id);
            return NoContent();
        }
    }
}
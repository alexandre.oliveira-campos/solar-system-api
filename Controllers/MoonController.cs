using Microsoft.AspNetCore.Mvc;
using SolarSystemApi.Models;
using SolarSystemApi.Services;

namespace SolarSystemApi.Controllers
{
    [ApiController]
    public class MoonController : ControllerBase
    {
        private readonly IMoonService _moonService;

        public MoonController(IMoonService moonService)
        {
            _moonService = moonService;
        }
        
        [HttpGet("moons")]
        public IActionResult GetAll()
        {
            return Ok(_moonService.GetAll());
        }
        
        [HttpGet("moons/{id}")]
        public IActionResult GetSingle(int id)
        {
            if (id < 1) return BadRequest();
            var moon = _moonService.GetSingle(id);
            if (moon == null) return NotFound();
            return Ok(moon);
        }
        
        [HttpPost("moons/new")]
        public IActionResult AddMoon(Moon newMoon)
        {
            if (newMoon.Id < 1) return BadRequest();
            //if (_moonService.ExistsByName(newMoon.Name)) return BadRequest();
            //if (_moonService.ExistsById(newMoon.Id)) return BadRequest();

            _moonService.AddMoon(newMoon);

            return Created($"moons/{newMoon.Id}/edit", newMoon);
        }
        
        [HttpPost("moons/{id}/edit")]
        public IActionResult Update([FromRoute] int id, [FromBody] Moon newMoon)
        {
            if (id < 1) return BadRequest();
            //if (_moonService.ExistsByName(newMoon.Name)) return BadRequest();
            //if (!_moonService.ExistsById(id)) return NotFound();

            return Ok(_moonService.Update(id, newMoon));
        }
        
        [HttpDelete("moons/{id}/delete")]
        public IActionResult Delete(int id)
        {
            _moonService.Delete(id);
            return NoContent();
        }
    }
}
using Microsoft.AspNetCore.Mvc;
using SolarSystemApi.Models;
using SolarSystemApi.Services;

namespace SolarSystemApi.Controllers
{
    [ApiController]
    public class PlanetController : ControllerBase
    {
        private readonly IPlanetService _planetService;

        public PlanetController(IPlanetService planetService)
        {
            _planetService = planetService;
        }
        
        [HttpGet("planets")]
        public IActionResult GetAll()
        {
            return Ok(_planetService.GetAll());
        }
        
        [HttpGet("planets/{id}")]
        public IActionResult GetSingle(int id)
        {
            if (id < 1) return BadRequest();
            var planet = _planetService.GetSingle(id);
            if (planet == null) return NotFound();
            return Ok(planet);
        }
        
        [HttpPost("planets/new")]
        public IActionResult AddPlanet(Planet newPlanet)
        {
            if (newPlanet.Id < 1) return BadRequest();
            //if (_planetService.ExistsByName(newPlanet.Name)) return BadRequest();
            //if (_planetService.ExistsById(newPlanet.Id)) return BadRequest();

            _planetService.AddPlanet(newPlanet);

            return Created($"planets/{newPlanet.Id}/edit", newPlanet);
        }
        
        [HttpPost("planets/{id}/edit")]
        public IActionResult Update([FromRoute] int id, [FromBody] Planet newPlanet)
        {
            if (id < 1) return BadRequest();
            //if (_planetService.ExistsByName(newPlanet.Name)) return BadRequest();
            //if (!_planetService.ExistsById(id)) return NotFound();

            return Ok(_planetService.Update(id, newPlanet));
        }
        
        [HttpDelete("planets/{id}/delete")]
        public IActionResult Delete(int id)
        {
            _planetService.Delete(id);
            return NoContent();
        }
    }
}
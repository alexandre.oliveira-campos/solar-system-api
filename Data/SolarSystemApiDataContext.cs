using Microsoft.EntityFrameworkCore;
using SolarSystemApi.Models;

namespace SolarSystemApi.Data
{
    public class SolarSystemApiDataContext : DbContext
    {
        public DbSet<SolarSystem> SolarSystems { get; set; }
        public DbSet<Planet> Planets { get; set; }
        public DbSet<Moon> Moons { get; set; }

        public SolarSystemApiDataContext(DbContextOptions<SolarSystemApiDataContext> options)
            : base(options)
        {
            
        }
    }
}
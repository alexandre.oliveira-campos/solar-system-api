﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SolarSystemApi.Migrations
{
    public partial class AddPlanet2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planet_SolarSystems_SolarSystemId",
                table: "Planet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Planet",
                table: "Planet");

            migrationBuilder.RenameTable(
                name: "Planet",
                newName: "Planets");

            migrationBuilder.RenameIndex(
                name: "IX_Planet_SolarSystemId",
                table: "Planets",
                newName: "IX_Planets_SolarSystemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Planets",
                table: "Planets",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets",
                column: "SolarSystemId",
                principalTable: "SolarSystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Planets",
                table: "Planets");

            migrationBuilder.RenameTable(
                name: "Planets",
                newName: "Planet");

            migrationBuilder.RenameIndex(
                name: "IX_Planets_SolarSystemId",
                table: "Planet",
                newName: "IX_Planet_SolarSystemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Planet",
                table: "Planet",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Planet_SolarSystems_SolarSystemId",
                table: "Planet",
                column: "SolarSystemId",
                principalTable: "SolarSystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SolarSystemApi.Migrations
{
    public partial class AddPropertiesPlanet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "Planets",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Size",
                table: "Planets",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color",
                table: "Planets");

            migrationBuilder.DropColumn(
                name: "Size",
                table: "Planets");
        }
    }
}

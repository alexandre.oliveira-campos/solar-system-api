﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SolarSystemApi.Migrations
{
    public partial class AddMoon2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Moon_Planets_PlanetId",
                table: "Moon");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Moon",
                table: "Moon");

            migrationBuilder.RenameTable(
                name: "Moon",
                newName: "Moons");

            migrationBuilder.RenameIndex(
                name: "IX_Moon_PlanetId",
                table: "Moons",
                newName: "IX_Moons_PlanetId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Moons",
                table: "Moons",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Moons_Planets_PlanetId",
                table: "Moons",
                column: "PlanetId",
                principalTable: "Planets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Moons_Planets_PlanetId",
                table: "Moons");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Moons",
                table: "Moons");

            migrationBuilder.RenameTable(
                name: "Moons",
                newName: "Moon");

            migrationBuilder.RenameIndex(
                name: "IX_Moons_PlanetId",
                table: "Moon",
                newName: "IX_Moon_PlanetId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Moon",
                table: "Moon",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Moon_Planets_PlanetId",
                table: "Moon",
                column: "PlanetId",
                principalTable: "Planets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

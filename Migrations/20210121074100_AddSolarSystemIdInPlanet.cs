﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SolarSystemApi.Migrations
{
    public partial class AddSolarSystemIdInPlanet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets");

            migrationBuilder.AlterColumn<int>(
                name: "SolarSystemId",
                table: "Planets",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets",
                column: "SolarSystemId",
                principalTable: "SolarSystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets");

            migrationBuilder.AlterColumn<int>(
                name: "SolarSystemId",
                table: "Planets",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_Planets_SolarSystems_SolarSystemId",
                table: "Planets",
                column: "SolarSystemId",
                principalTable: "SolarSystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
